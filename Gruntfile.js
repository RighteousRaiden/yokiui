module.exports = function(grunt) {

  grunt.initConfig({
    
    // Less formatting
    less: {
      dist:{
        files: {
          "production/assets/css/custom-style.css": "production/assets/less/custom-style.less"
        }
      }
    },
      
    // Concat Formatting
    concat: {
      js: {
        src: ['production/assets/js/angular.js', 'production/assets/js/jquery.js'],
        dest: 'public/javascripts/scripts.js',
      },
      css: {
        src: ['bower_components/normalize-css/*.css', 'production/assets/css/style.css', 'production/assets/css/custom-style.css'],
        dest: 'public/stylesheets/style.css',
      }
    },

    // Cssmin formatting
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'public/stylesheets/',
          src: ['style.css', '!*.min.css'],
          dest: 'public/stylesheets/',
          ext: '.min.css'
        }]
      }
    },

    // Uglify js
    uglify: {
      dist: {
        files: {
          'public/javascripts/scripts.min.js': ['public/javascripts/scripts.js']
        }
      }
    },

    // Jshint
    jshint: {
      all: ['Gruntfile.js', 'production/']
    },

    // Watch formatting
    watch: {
      scripts: {
        files: 'production/**/*',
        tasks: ['less', 'concat', 'cssmin', 'uglify', 'jshint'],
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['watch']);

};
