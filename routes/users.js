/////////////////////////////////////////////////////////////////
// Modules Calls
/////////////////////////////////////////////////////////////////
var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var app = express();
var fs = require('fs');
var bodyParser = require('body-parser');

/////////////////////////////////////////////////////////////////
// Get User
/////////////////////////////////////////////////////////////////
var User = require('../models/user');
var Setting = require('../models/setting');

/////////////////////////////////////////////////////////////////
// Console Log for /
/////////////////////////////////////////////////////////////////
router.get('/', function(req, res, next) {
	res.redirect('/');
});


/////////////////////////////////////////////////////////////////
// Console Log for /register
/////////////////////////////////////////////////////////////////
router.get('/register', ensureAuthenticated, function(req, res, next) {
    console.log('GET request sent to register');
	User.find(function(err, User) {
		if (err) return next(err);
		res.render('register', { 
        title: 'Register',
		metaKeywords: 'Yokiui Registration, Yokiui',
		User: User
	  });   
	});
});
/////////////////////////////////////////////////////////////////
// Console Log for /register
/////////////////////////////////////////////////////////////////
router.get('/demoregister', function(req, res, next) {
    console.log('GET request sent to register');
    res.render('register', { 
        title: 'Demo Register',
		metaKeywords: 'Yokiui Demo Registration, Yokiui'
  });   
});

/////////////////////////////////////////////////////////////////
// Console Log for /login
/////////////////////////////////////////////////////////////////
router.get('/login', function(req, res, next) {
    console.log('GET request sent to login');
    res.render('login', { 
        title: 'Demo Login',
		metaKeywords: 'Yokiui Login, Yokiui'
  });
});

/////////////////////////////////////////////////////////////////
// Console Log for /profile
/////////////////////////////////////////////////////////////////
router.get('/profile',  ensureAuthenticated, function(req, res, next) {
    console.log('GET request sent to admin');
    res.render('profile', { 
        title: 'profile',
		metaKeywords: 'Yokiui Profile, Yokiui'
  }); 
});

function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	}
	res.redirect('/users/login');
}

/////////////////////////////////////////////////////////////////
// Posting register data
/////////////////////////////////////////////////////////////////
router.post('/register', function(req, res, next){
	/////////////////////////////////////////////////////////////////
	// Form values
	/////////////////////////////////////////////////////////////////
	var name = req.body.name;
	var email = req.body.email;
	var username = req.body.username;
	var password = req.body.password;
	var password2 = req.body.password2;
	
	console.log('User being created...');
	console.log('First Name: ' + req.body.name);
	console.log('Email: ' + req.body.email);
	console.log('Username: ' + req.body.username);
	console.log('Password: ' + req.body.password);
	console.log('Password Match: ' + req.body.password2);
	if (password == password2){
		console.log('Passwords match!');
	}else{
		console.log('Passwords do not match!');
		console.log('Registration failed...');
	}


/////////////////////////////////////////////////////////////////
// Image field TBF
/////////////////////////////////////////////////////////////////
if(req.body.profileimage){
	console.log("Uploading file...");
	/////////////////////////////////////////////////////////////////
	// File info
	/////////////////////////////////////////////////////////////////
	var profileImageOriginalName = req.files.profileimage.originalname;
	var profileImageName = req.files.profileimage.name;
	var profileImageMime = req.files.profileimage.mimetype;
	var profileImagePath = req.files.profileimage.path;
	var profileImageExt = req.files.profileimage.extension;
	var profileImageSize = req.files.profileimage.size;
} else{
	/////////////////////////////////////////////////////////////////
	// Default image
	/////////////////////////////////////////////////////////////////
	var profileImageName = 'noimage.png';
}

	/////////////////////////////////////////////////////////////////
	// Form Validation TBF
	/////////////////////////////////////////////////////////////////
	req.checkBody('name', 'Name field is required!').notEmpty();
	req.checkBody('email', 'Email required!').notEmpty();
	req.checkBody('email', 'Email not valid!').isEmail();
	req.checkBody('username', 'Username is required!').notEmpty();
	req.checkBody('password', 'Password required!').notEmpty();
	req.checkBody('password2', 'Password do not match!').equals(req.body.password);
	
	/////////////////////////////////////////////////////////////////
	// Error checks
	/////////////////////////////////////////////////////////////////
	var errors = req.validationErrors();
	
	if(errors){
		res.render('register',{
			errors: errors,
			name: name,
			email: email,
			username: username,
			password: password,
			password2: password2
		});
	} else{
			var newUser = new User({
			name: name,
			email: email,
			username: username,
			password: password,
			profileimage: profileImageName
		});
		
		
		
		/////////////////////////////////////////////////////////////////
		// Create user
		/////////////////////////////////////////////////////////////////
		User.createUser(newUser, function(err, user){
			if(err) throw err;
			console.log(user);
		});
		
		/////////////////////////////////////////////////////////////////
		// Success Message
		/////////////////////////////////////////////////////////////////
		req.flash("success", "You are now registed and may log in")
		res.location('/admin');
		res.redirect('/admin');
		
	}
});

/////////////////////////////////////////////////////////////////
// Session data
/////////////////////////////////////////////////////////////////
passport.serializeUser(function(user, done){
	done(null, user.id);							 
});

passport.deserializeUser(function(id, done){
	User.getUserById(id, function(err, user){
		done(err, user);
	});
});

/////////////////////////////////////////////////////////////////
// Passport authentication
/////////////////////////////////////////////////////////////////
passport.use(new LocalStrategy(
	function(username, password, done){
		User.getUserByUsername(username, function(err, user){
			if(err) throw err;
			if(!user){
				console.log('Unknown User');
				return done(null, false, {message:'unknown user'})
			}
			
			User.comparePassword(password, user.password, function(err, isMatch){
				if(err)throw err;
				if(isMatch){
					return done(null, user);
				} else{
					console.log('Invalid Password');
					return done(null, false, {message:'Invalid Password'});
				}
			});
		});
	}
));

router.post('/login', passport.authenticate('local', {failureRedirect:'/users/login', failureFlash:'Invalid username or password'}), function(req, res){
	console.log('Authentication Successful');
	req.flash("success", "You are logged in!");
	res.location('/admin');
	res.redirect('/admin');
});

router.get('/logout', function(req, res) {
   	req.logout();
	console.log('Logout Successful');
	req.flash("success", "You have logged out!");
	res.redirect('/');
});


 //User.find({}, function(err, data){
 	//console.log(">>>> " + data );
 //});

module.exports = router;