/////////////////////////////////////////////////////////////////
// Module calls
/////////////////////////////////////////////////////////////////
var express = require('express');
var router = express.Router();
var app = express();
var fs = require('fs');
var bodyParser = require('body-parser');

var UserSetting = require('../models/setting');


/////////////////////////////////////////////////////////////////
// Homepage contents
/////////////////////////////////////////////////////////////////

router.get('/', function(req, res, next) {
    console.log('GET request sent to homepage');
	UserSetting.find(function(err, UserSetting) {
		if (err) return next(err);
		res.render('index', { 
			title: 'Home',
			metaKeywords: 'Yokiui Home, Yokiui',
			UserSetting: UserSetting
		}); 
	});
});

/////////////////////////////////////////////////////////////////
// Admin page contents
/////////////////////////////////////////////////////////////////

router.get('/admin',  ensureAuthenticated, function(req, res, next) {
	console.log('GET request sent to admin');		
		res.render('admin', { 
		title: 'Admin',
		metaKeywords: 'Yokiui Admin, Yokiui'
  }); 
});


/////////////////////////////////////////////////////////////////
// Ensure Authentication
/////////////////////////////////////////////////////////////////
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	}
	res.redirect('/users/login');
}

/////////////////////////////////////////////////////////////////
// Post Requests TBF
/////////////////////////////////////////////////////////////////

router.post('/admin', function(req, res, next){
	/////////////////////////////////////////////////////////////////
	// Form Values
	/////////////////////////////////////////////////////////////////
	var cssTextareaInput = req.body.cssTextareaInput;
	var appendLogoImg = req.body.appendLogoImg;
	var appendLogoLink = req.body.appendLogoLink;
	var appendJumbotronHeader = req.body.appendJumbotronHeader;
	var appendJumbotronParagraph = req.body.appendJumbotronParagraph;
	var appendMainContentHeader = req.body.appendMainContentHeader;
	var appendMainContentParagraph = req.body.appendMainContentParagraph;
	var appendFooterSection = req.body.appendFooterSection;
	var appendFooterLink = req.body.appendFooterLink;


	/////////////////////////////////////////////////////////////////
	// Form Validation NEEDS FIXED
	/////////////////////////////////////////////////////////////////
	//req.checkBody('cssTextareaInput', 'cssTextareaInput: Content required!').notEmpty();
	//req.checkBody('appendLogoImg', 'appendLogoImg: Content required!').notEmpty();
	//req.checkBody('appendLogoLink', 'appendLogoLink: Content required!').notEmpty();
	//req.checkBody('appendJumbotronHeader', 'appendJumbotronHeader: Content required!').notEmpty();
	//req.checkBody('appendJumbotronParagraph', 'appendJumbotronParagraph: Content required!').notEmpty();
	//req.checkBody('appendMainContentHeader', 'appendMainContentHeader: Content required!').notEmpty();
	//req.checkBody('appendMainContentParagraph', 'appendMainContentParagraph: Content required!').notEmpty();
	//req.checkBody('appendFooterSection', 'appendFooterSection: Content required!').notEmpty();
	//req.checkBody('appendFooterLink', 'appendFooterLink: Content required!').notEmpty();
	
	/////////////////////////////////////////////////////////////////
	// Error Checks
	/////////////////////////////////////////////////////////////////
	var errors = req.validationErrors();
	if(errors){
		res.render('admin',{
			errors: errors,
			cssTextareaInput: cssTextareaInput,
			appendLogoImg: appendLogoImg,
			appendLogoLink: appendLogoLink,
			appendJumbotronHeader: appendJumbotronHeader,
			appendJumbotronParagraph: appendJumbotronParagraph,
			appendMainContentHeader: appendMainContentHeader,
			appendMainContentParagraph: appendMainContentParagraph,
			appendFooterSection: appendFooterSection,
			appendFooterLink: appendFooterLink
		});
	} else{
			var newUserSetting = new UserSetting({
			cssTextareaInput: cssTextareaInput,
			appendLogoImg: appendLogoImg,
			appendLogoLink: appendLogoLink,
			appendJumbotronHeader: appendJumbotronHeader,
			appendJumbotronParagraph: appendJumbotronParagraph,
			appendMainContentHeader: appendMainContentHeader,
			appendMainContentParagraph: appendMainContentParagraph,
			appendFooterSection: appendFooterSection,
			appendFooterLink: appendFooterLink
		});
		
		
		
		/////////////////////////////////////////////////////////////////
		// Create Setting
		/////////////////////////////////////////////////////////////////
		UserSetting.createUserSetting(newUserSetting, function(err, UserSetting){
			if(err) throw err;
			console.log(UserSetting);
		});
		
		/////////////////////////////////////////////////////////////////
		// Success Message
		/////////////////////////////////////////////////////////////////
		req.flash("success", "Settings Added")
		res.location('/admin');
		res.redirect('/admin');
		
	}
});

module.exports = router;



