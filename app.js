/////////////////////////////////////////////////////////////////
// Core Modules
/////////////////////////////////////////////////////////////////
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var expressValidator = require('express-validator');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bodyParser = require('body-parser');
var multer = require('multer');
var flash = require('connect-flash');
var mongo = require('mongodb');
var mongoose = require('mongoose');

var db = mongoose.connection;
var app = express();

/////////////////////////////////////////////////////////////////
// Parse application/x-www-form-urlencoded
/////////////////////////////////////////////////////////////////
app.use(bodyParser.urlencoded({extended: false}));

/////////////////////////////////////////////////////////////////
// Express Validator
/////////////////////////////////////////////////////////////////
app.use(expressValidator({
	errorFormatter: function(param, msg, value){
		var namespace = param.split('.')
		, root = namespace.shift()
		, formParam = root;
		while(namespace.length){
			formParam += '[' + namespace.shift() + ']'
		}
		return{
			param: formParam,
			msg: msg,
			value: value
		};	
	}
}));

/////////////////////////////////////////////////////////////////
// Log when application is running
/////////////////////////////////////////////////////////////////
console.log ("Listening on Port 3000");
console.log ("Site bound to: http://localhost:3000/");

/////////////////////////////////////////////////////////////////
// Routes Setup
/////////////////////////////////////////////////////////////////
var routes = require('./routes/index');
var users = require('./routes/users');


/////////////////////////////////////////////////////////////////
// View Engine setup
/////////////////////////////////////////////////////////////////
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade'); //Formerly ejs

/////////////////////////////////////////////////////////////////
// File uploads handler
/////////////////////////////////////////////////////////////////
app.use(multer({dest:'./uploads/'}).single('profileimage'));

/////////////////////////////////////////////////////////////////
// Uncomment after placing favicon in /public
/////////////////////////////////////////////////////////////////
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/////////////////////////////////////////////////////////////////
// Express Sessions handler
/////////////////////////////////////////////////////////////////
app.use(session({
	secret:'secret',
	saveUninitialized: true,
	resave: true
}));

/////////////////////////////////////////////////////////////////
// Passport
/////////////////////////////////////////////////////////////////
app.use(passport.initialize());
app.use(passport.session());

/////////////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////////////
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/////////////////////////////////////////////////////////////////
// Flash Messages
/////////////////////////////////////////////////////////////////
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

app.get('*', function(req, res, next){
	res.locals.user = req.user || null; /* Ability to call user locally to check if user is logged in or not. */
	next();
});


/////////////////////////////////////////////////////////////////
// End routes
/////////////////////////////////////////////////////////////////
app.use('/', routes);
app.use('/users', users);

/////////////////////////////////////////////////////////////////
// 404 Error Handler
/////////////////////////////////////////////////////////////////
app.use(function(req, res, next) {
  var err = new Error('Sorry, we could not find that page');
  err.status = 404;
  next(err);
});

/////////////////////////////////////////////////////////////////
// Other error handlers
/////////////////////////////////////////////////////////////////

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;