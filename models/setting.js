var mongoose = require('mongoose');
var uri = 'mongodb://localhost/yokiui?poolSize=4';
mongoose.createConnection(uri);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
	console.log('Conntected To Mongo Database: UserSettings');
});


/////////////////////////////////////////////////////////////////
// Setting Schema
/////////////////////////////////////////////////////////////////
var UserSettingSchema = mongoose.Schema({
	cssTextareaInput:{
		type: String,
		index: true
	},
	appendLogoImg:{
		type: String,
	},
	appendLogoLink:{
		type: String,
	},
	appendJumbotronHeader:{
		type: String,
	},
	appendJumbotronParagraph:{
		type: String,
	},
	appendMainContentHeader:{
		type: String,
	},
	appendMainContentParagraph:{
		type: String,
	},
	appendFooterSection:{
		type: String,
	},
	appendFooterLink:{
		type: String,
	},
});

var UserSetting = module.exports = mongoose.model('UserSetting', UserSettingSchema);


/////////////////////////////////////////////////////////////////
// Default setting to avoid callback issues.
/////////////////////////////////////////////////////////////////
var defaultSetting = new UserSetting({ 
	cssTextareaInput:'body{}',
	appendLogoImg:'https://yt3.ggpht.com/-bxbElO80mPw/AAAAAAAAAAI/AAAAAAAAAAA/Un_5iXoePC4/s100-c-k-no/photo.jpg',
	appendLogoLink:'http://www.stevenmckaylowry.co.uk',
	appendJumbotronHeader:'Jumbotron Header Text.',
	appendJumbotronParagraph:'Jumbotron Paragraph Text.',
	appendMainContentHeader:'Main Content Header Text.',
	appendMainContentParagraph:'Main Content POaragraph Text.',
	appendFooterSection:'appendFooterSection',
	appendFooterLink:'appendFooterLink'
});

defaultSetting.save(function(err, defaultSetting) {
  if (err) return console.error(err);
	console.log('Default UserSettings added.');
});

module.exports.createUserSetting = function(newUserSetting, callback){
	newUserSetting.save(callback);
};


//Setting.find({}, function(err, setting){
	//console.log(">>>> " + setting[0].appendFooterLink);
//});