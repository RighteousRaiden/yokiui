/////////////////////////////////////////////////////////////////
// JQuery Contents
/////////////////////////////////////////////////////////////////
console.log('Jquery JS added.');

/////////////////////////////////////////////////////////////////
// Timer for nose messages
/////////////////////////////////////////////////////////////////
setTimeout(function() {
    $(".container div#messages ul").slideUp(1000);
}, 6000);

/////////////////////////////////////////////////////////////////
// Setting base value of count for each button
/////////////////////////////////////////////////////////////////
var intId = $("#displaySettings div").length + 0;
var previewId = $(".container div#livePreview div").length + 0;
var errorCount = $(".container div#custommessages ul").length + 0;
var cssCount = $("#displaySettings #appendCss").length + 0;
var logoCount = $("#displaySettings #appendLogo").length + 0;
var logoImgCount = $("#displaySettings .logoContainer #appendLogoImg").length + 0;
var logoLinkCount = $("#displaySettings .logoContainer #appendLogoLink").length + 0;
var jumbotronCount = $("#displaySettings #appendJumbotron").length + 0;
var jumbotronHeaderCount = $("#displaySettings .jumbotronContainer #appendJumbotronHeader").length + 0;
var jumbotronParagraphCount = $("#displaySettings .jumbotronContainer #appendJumbotronParagraph").length + 0;
var mainContentCount = $("#displaySettings #appendMainContent").length + 0;
var mainContentHeaderCount = $("#displaySettings .mainContentContainer #appendMainContentHeader").length + 0;
var mainContentParagraphCount = $("#displaySettings .mainContentContainer #appendMainContentParagraph").length + 0;
var footerCount = $("#displaySettings #appendFooter").length + 0;
var footerSectionCount = $("#displaySettings .footercontainer #appendFooterSection").length + 0;
var footerLinkCount = $("#displaySettings .footercontainer #appendFooterLink").length + 0;

var cssPreviewCount = $("style").length + 0;
var jumbotronHeaderPreviewCount = $("#livePreview .container .jumbotron h1").length + 0;
var jumbotronParagraphPreviewCount = $("#livePreview .container .jumbotron p").length + 0;
var mainContentHeaderPreviewCount = $("#livePreview .container .mainContent h1").length + 0;
var mainContentParagraphPreviewCount = $("#livePreview .container .mainContent p").length + 0;
var mainContentFooterSectionCount = $("#livePreview .container .footerPreview h1").length + 0;
var mainContentFooterLinkCount = $("#livePreview .container .footerPreview p").length + 0;



console.clear();
console.log("Please. Turn off \'preserve log.\'  ^^^^^^^^^^^^");
console.log('intID base value = ' + intId);
console.log('previewId base value = ' + previewId);

/////////////////////////////////////////////////////////////////
// Setting
/////////////////////////////////////////////////////////////////
$("a#appendLogoImg").addClass("disabled");
$("a#appendLogoLink").addClass("disabled");
$("a#appendJumbotronHeader").addClass("disabled");
$("a#appendJumbotronParagraph").addClass("disabled");
$("a#appendMainContentHeader").addClass("disabled");
$("a#appendMainContentParagraph").addClass("disabled");
$("a#appendFooterSection").addClass("disabled");
$("a#appendFooterLink").addClass("disabled");

/////////////////////////////////////////////////////////////////
// Custom CSS
/////////////////////////////////////////////////////////////////
$("a#appendCss").click(function() {
    if (cssCount < 1) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        cssCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container cssContainer\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h3>Custom CSS</h3></div>");
        var fieldLabel = $("<label for=\"Logo-set" + intId + "\">Custom CSS: </label>");
        var fieldInput = $("<textarea class=\"fieldname\" id=\"cssTextareaInput\" name=\"cssTextareaInput\"></textarea>");
        var previewButton = $("<button type=\"button\" id=\"addCss\">Preview Css</button> ");
        var clearButton = $("<button type=\"button\" id=\"removeCss\">Clear Css</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //Removes parent
            intId--; //Count added
            cssCount = 0; //Reset logo count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<unappendCss>');
            $(this).parent().remove();
            console.log('- IntId: ' + intId);
            console.log('- cssCount: ' + cssCount);
            console.log('</unappendCss>');
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        fieldWrapper.append(fieldLabel);
        fieldWrapper.append(fieldInput);
        fieldWrapper.append(previewButton);
        fieldWrapper.append(clearButton);
        $("#displaySettings").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console logs
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendCss>');
        console.log('- IntId: ' + intId);
        console.log('- cssCount: ' + cssCount);
        console.log('</appendCss>');

        /////////////////////////////////////////////////////////////////
        // Append to page
        /////////////////////////////////////////////////////////////////
        $("button#addCss").click(function() {
            var cssData = $("#cssTextareaInput").val();
            console.log(cssData);
            if (cssPreviewCount < 2) {
                $('head').append("<style type=\"text/css\">" + cssData + "</style>");
                console.clear();
                cssPreviewCount++;
                console.log('<addCSS>');
                console.log('- Following Styling applied: ');
                if (cssData === "") {
                    console.log('No Css Applied...');
                    var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>Please fill in the Custom CSS Field!'</li> </ul>");
                    $(".container div#custommessages").append(errorWrapper);
                    setTimeout(function() {
                        $(".container div#custommessages ul").slideUp(1000);
                    }, 6000);
                    errorCount++;
                    console.clear();
                    console.log("Error triggered, trigger count = " + errorCount);
                    cssPreviewCount--;
                } else {
                    console.log(cssData);
                }
                console.log('</addCss>');
            } else {
                var errorWrapper2 = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add 1 Custom CSS Preview!'</li> </ul>");
                $(".container div#custommessages").append(errorWrapper2);
                setTimeout(function() {
                    $(".container div#custommessages ul").slideUp(1000);
                }, 6000);
                errorCount++;
                console.clear();
                console.log("Error triggered, trigger count = " + errorCount);
            }
        });

        /////////////////////////////////////////////////////////////////
        // unAppend from page
        /////////////////////////////////////////////////////////////////
        $("button#removeCss").click(function() {
            $('style').remove();
            cssPreviewCount = 1;
            console.clear();
            console.log('<removeCSS>');
            console.log('- Custom Css removed.');
            console.log('</removeCss>');
        });

    } else {
        /////////////////////////////////////////////////////////////////
        // Error display
        /////////////////////////////////////////////////////////////////
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + cssCount + " Custom CSS!'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendLogo
/////////////////////////////////////////////////////////////////
$("a#appendLogo").click(function() {
    if (logoCount < 1) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        logoCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container logoContainer\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h1>Logo Settings</h1></div>");
        var previewButton = $("<button type=\"button\" id=\"addCss\">Preview Css</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");


        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //Removes parent
            intId--; //Count added
            logoCount = 0; //Reset logo count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<unappendLogo>');
            $(this).parent().remove();
            console.log('- IntId: ' + intId);
            console.log('- logoCount: ' + logoCount);
            $("a#appendLogoImg").addClass("disabled");
            console.log('- Added disabled class to a#appendLogoImg');
            console.log('</unappendLogo>');
            console.log('<removeLogo>');
            $('.logoPreview').remove();
            previewId--;
            console.log('- previesId = ' + previewId);
            console.log('</removeLogo>');

            /////////////////////////////////////////////////////////////////
            // Logo image count check
            /////////////////////////////////////////////////////////////////
            if (logoImgCount > 0) {
                $(".logoImageContainer").remove();
                intId--;
                logoImgCount = 0;
                /////////////////////////////////////////////////////////////////
                // Console Log
                /////////////////////////////////////////////////////////////////
                console.clear();
                console.log('<unappendLogoImg>');
                console.log('- IntId: ' + intId);
                console.log('- logoImgCount: ' + logoImgCount);
                $("a#appendLogoLink").addClass("disabled");
                console.log('- Added disabled class to a#appendLogoLink');
                console.log('</unappendLogoImg>');
            } else {
                return;
            }
            /////////////////////////////////////////////////////////////////
            // Logo Link count check
            /////////////////////////////////////////////////////////////////
            if (logoLinkCount > 0) {
                $(".logoLinkContainer").remove();
                intId--;
                logoLinkCount = 0;
                /////////////////////////////////////////////////////////////////
                // Console Log
                /////////////////////////////////////////////////////////////////
                console.log('<unappendLogoLink>');
                console.log('- IntId: ' + intId);
                console.log('- logoLinkCount: ' + logoLinkCount);
                console.log('</unappendLogoLink>');
            } else {
                return;
            }
        });


        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        $("#displaySettings").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console logs
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendLogo>');
        console.log('- IntId: ' + intId);
        console.log('- logoCount: ' + logoCount);
        $("a#appendLogoImg").removeClass("disabled");
        console.log('- Removed disabled class from a#appendLogoImg');
        console.log('</appendLogo>');

        /////////////////////////////////////////////////////////////////
        // Live Preview Code
        /////////////////////////////////////////////////////////////////
        console.log('<addLogo>');
        previewId++;
        var previewWrapper = $("<div class=\"container logoPreview\" id=\"preview" + previewId + "\"/ >");
        var previewHolder = $("<div class='logoPlaceholder'><h1>LogoPreview</h1></div>");

        previewWrapper.append(previewHolder);
        $("#livePreview").append(previewWrapper);
        $('.logoPreview .logoPlaceholder').append('<a id="logoImageLink"><img/></div>');

        console.log('- Logo applied to preview.');
        console.log('- previewId = ' + previewId);
        console.log('</addLogo>');

    } else {
        /////////////////////////////////////////////////////////////////
        // Error display
        /////////////////////////////////////////////////////////////////
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + logoCount + " Logo!'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendLogoImg
/////////////////////////////////////////////////////////////////
$("a#appendLogoImg").click(function() {
    if (logoImgCount < 1) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        logoImgCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container logoImageContainer innerSetting\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h3>Logo Image Settings</h3></div>");
        var fieldLabel = $("<label for=\"Logo-set" + intId + "\">Logo Image URL: </label>");
        var fieldInput = $("<input type=\"text\" id=\"logoImgInput\" class=\"fieldname\" name=\"appendLogoImg\"/>");
        var previewButton = $("<button type=\"button\" id=\"addLogoImg\">Preview Logo Image URL</button> ");
        var clearButton = $("<button type=\"button\" id=\"removeLogoImg\">Clear Logo Image URL</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //Remove parent
            intId--; //take away from count
            logoImgCount = 0; //reset count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<unappendLogoImg>');
            console.log('- IntId: ' + intId);
            console.log('- logoImgCount: ' + logoImgCount);
            $("a#appendLogoLink").addClass("disabled");
            console.log('- Added disabled class to a#appendLogoLink');
            console.log('</unappendLogoImg>');
            /////////////////////////////////////////////////////////////////
            // Logo link count check
            /////////////////////////////////////////////////////////////////
            if (logoLinkCount > 0) {
                $(".logoLinkContainer").remove();
                intId--; //take away from count
                logoLinkCount = 0; //reset count
                /////////////////////////////////////////////////////////////////
                // Console Log
                /////////////////////////////////////////////////////////////////
                console.log('<unappendLogoLink>');
                console.log('- IntId: ' + intId);
                console.log('- logoLinkCount: ' + logoLinkCount);
                console.log('</unappendLogoLink>');
            } else {
                return;
            }
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        fieldWrapper.append(fieldLabel);
        fieldWrapper.append(fieldInput);
        fieldWrapper.append(previewButton);
        fieldWrapper.append(clearButton);
        $("#displaySettings .logoContainer").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendLogoImg>');
        console.log('- IntId: ' + intId);
        console.log('- logoImgCount: ' + logoImgCount);
        $("a#appendLogoLink").removeClass("disabled");
        console.log('- Removed disabled class from a#appendLogoLink');
        console.log('</appendLogoImg>');

        /////////////////////////////////////////////////////////////////
        // Live Preview code
        /////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////
        // Append to preview
        /////////////////////////////////////////////////////////////////
        $("button#addLogoImg").click(function() {
            var imgInput = $("#logoImgInput").val();
            console.clear();
            console.log('<addLogoImg>');
            console.log('Logo Image applied:');
            if (imgInput === "") {
                console.log('Image URL is empty...');
            } else {
                console.log(imgInput);
            }
            console.log('<addLogoImg>');
            $('.logoPreview .logoPlaceholder a#logoImageLink img').attr({
                src: "http://" + imgInput
            });
        });

        /////////////////////////////////////////////////////////////////
        // unAppend from preview
        /////////////////////////////////////////////////////////////////
        $("button#removeLogoImg").click(function() {
            $('.logoPreview .logoPlaceholder a#logoImageLink img').attr({
                src: ''
            });
            console.clear();
            console.log('<addLogoImg>');
            console.log('Logo Image cleared:');
            console.log('<addLogoImg>');
        });

    } else {
        /////////////////////////////////////////////////////////////////
        // Error display
        /////////////////////////////////////////////////////////////////
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + logoImgCount + " Logo image!'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendLogoLink
/////////////////////////////////////////////////////////////////
$("a#appendLogoLink").click(function() {
    if (logoLinkCount < 1) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        logoLinkCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container logoLinkContainer innerSetting\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h3>Logo Image Settings</h3></div>");
        var fieldLabel = $("<label for=\"Logo-set" + intId + "\">Logo Link URL: </label>");
        var fieldInput = $("<input type=\"text\" id=\"logoImgLinkInput\" class=\"fieldname\" name=\"appendLogoLink\"/>");
        var previewButton = $("<button type=\"button\" id=\"addLogoImgLink\">Preview Logo Image Link</button> ");
        var clearButton = $("<button type=\"button\" id=\"removeLogoImgLink\">Clear Logo Image Link</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            logoLinkCount = 0; //reset count
            console.clear();
            console.log('<unappendLogoLink>');
            console.log('- IntId: ' + intId);
            console.log('- logoLinkCount: ' + logoLinkCount);
            console.log('</unappendLogoLink>');
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        fieldWrapper.append(fieldLabel);
        fieldWrapper.append(fieldInput);
        fieldWrapper.append(previewButton);
        fieldWrapper.append(clearButton);
        $("#displaySettings .logoContainer").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console logs
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendLogoLink>');
        console.log('- IntId: ' + intId);
        console.log('- logoLinkCount: ' + logoLinkCount);
        console.log('</appendLogoLink>');

        /////////////////////////////////////////////////////////////////
        // Live Preview code
        /////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////
        // Append to preview
        /////////////////////////////////////////////////////////////////
        $("button#addLogoImgLink").click(function() {
            var imgLinkInput = $("#logoImgLinkInput").val();
            console.log('<addLogoLink>');
            console.log('Logo Image Link applied:');
            if (imgLinkInput === "") {
                console.log('Image Link is empty...');
            } else {
                console.log(imgLinkInput);
            }
            console.log('</addLogoLink>');
            $('.logoPreview .logoPlaceholder a#logoImageLink').attr({
                href: 'http://' + imgLinkInput
            });
        });

        /////////////////////////////////////////////////////////////////
        // unAppend from preview
        /////////////////////////////////////////////////////////////////
        $("button#removeLogoImgLink").click(function() {
            console.log('<removeLogoLink>');
            $('.logoPreview .logoPlaceholder a#logoImageLink').attr({
                href: ''
            });
            console.log('- Logo image link removed.');
            console.log('</removeLogoLink>');
        });

    } else {
        /////////////////////////////////////////////////////////////////
        // Error display
        /////////////////////////////////////////////////////////////////
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + logoLinkCount + " Logo image links!'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendJumbotron
/////////////////////////////////////////////////////////////////
$("a#appendJumbotron").click(function() {
    if (jumbotronCount < 1) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        jumbotronCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container jumbotronContainer\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h1>Jumbotron Settings</h1></div>");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            jumbotronCount = 0; //reset count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<unappendJumbotron>');
            console.log('- IntId: ' + intId);
            console.log('- jumbotronCount: ' + jumbotronCount);
            $("a#appendJumbotronHeader").addClass("disabled");
            $("a#appendJumbotronParagraph").addClass("disabled");
            console.log('- Added disabled class to a#appendJumbotronHeader & a#appendJumbotronParagraph');
            console.log('</unappendJumbotron>');
            console.log('<removeJumbotron>');
            $('.jumbotronPreview').remove();
            previewId--;
            console.log('- previesId = ' + previewId);
            console.log('</removeJumbotron>');
            /////////////////////////////////////////////////////////////////
            // jumbotron header count check
            /////////////////////////////////////////////////////////////////
            if (jumbotronHeaderCount > 0) {
                $(".jumbotronHeaderContainer").remove();
                intId--;
                jumbotronHeaderCount = 0;
                /////////////////////////////////////////////////////////////////
                // Console Log
                /////////////////////////////////////////////////////////////////
                console.log('<unappendJumbotronHeader>');
                console.log('- IntId: ' + intId);
                console.log('- jumbotronHeaderCount: ' + jumbotronHeaderCount);
                console.log('</unappendJumbotronHeader>');
            } else {
                return;
            }
            if (jumbotronParagraphCount > 0) {
                $(".jumbotronParagraphContainer").remove();
                /////////////////////////////////////////////////////////////////
                // Console Log
                /////////////////////////////////////////////////////////////////
                console.log('<unappendJumbotronParagraph>');
                for (i = 0; i < jumbotronParagraphCount; i++) {
                    intId--;
                    console.log('- Removing jumbotron paragraphs');
                }
                console.log('- IntId: ' + intId);
                jumbotronParagraphCount = 0;
                console.log('- jumbotronParagraphCount: ' + jumbotronParagraphCount);
                console.log('</unappendJumbotronParagraph>');
            } else {
                return;
            }
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        $("#displaySettings").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendJumbotron>');
        console.log('- IntId: ' + intId);
        console.log('- jumbotronCount: ' + jumbotronCount);
        $("a#appendJumbotronHeader").removeClass("disabled");
        $("a#appendJumbotronParagraph").removeClass("disabled");
        console.log('- Removed disabled class from a#appendJumbotronHeader & a#appendJumbotronParagraph');
        console.log('</appendJumbotron>');

        /////////////////////////////////////////////////////////////////
        // Live Preview Code
        /////////////////////////////////////////////////////////////////
        console.log('<addJumbotron>');
        previewId++;
        var previewWrapper = $("<div class=\"container jumbotronPreview\" id=\"preview" + previewId + "\"/ >");
        var previewHolder = $("<div class='jumbotron'></div>");

        previewWrapper.append(previewHolder);
        $("#livePreview").append(previewWrapper);

        console.log('- Jumbotron applied to preview.');
        console.log('- previewId = ' + previewId);
        console.log('</addJumbotron>');


    } else {
        /////////////////////////////////////////////////////////////////
        // Error display
        /////////////////////////////////////////////////////////////////
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + jumbotronCount + " Jumbotron!'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendJumbotronHeader
/////////////////////////////////////////////////////////////////
$("a#appendJumbotronHeader").click(function() {
    if (jumbotronHeaderCount < 1) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        jumbotronHeaderCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container jumbotronHeaderContainer innerSetting\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h3>Jumbotron Header Settings</h3></div>");
        var fieldLabel = $("<label for=\"Logo-set" + intId + "\">Jumbotron Header Settings: </label>");
        var fieldInput = $("<input type=\"text\" id=\"jumbotronHeaderInput\"/ class=\"fieldname\" name=\"appendJumbotronHeader\"/>");
        var previewButton = $("<button type=\"button\" id=\"addJumbotronHeader\">Preview Jumbotron Header</button> ");
        var clearButton = $("<button type=\"button\" id=\"removeJumbotronHeader\">Clear Jumbotron Header</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            jumbotronHeaderCount = 0; //reset count
            console.clear();
            console.log('<unappendJumbotronHeader>');
            console.log('- IntId: ' + intId);
            console.log('- jumbotronHeaderCount: ' + jumbotronHeaderCount);
            console.log('</unappendJumbotronHeader>');
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        fieldWrapper.append(fieldLabel);
        fieldWrapper.append(fieldInput);
        fieldWrapper.append(previewButton);
        fieldWrapper.append(clearButton);
        $("#displaySettings .jumbotronContainer").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendJumbotronHeader>');
        console.log('- IntId: ' + intId);
        console.log('- jumbotronHeadercount: ' + jumbotronHeaderCount);
        console.log('</appendJumbotronHeader>');

        /////////////////////////////////////////////////////////////////
        // Live Preview code
        /////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////
        // Append to preview
        /////////////////////////////////////////////////////////////////
        $("button#addJumbotronHeader").click(function() {
            var jumbotronHeaderInput = $("#jumbotronHeaderInput").val();
            if (jumbotronHeaderPreviewCount === 0) {
                $('.container .jumbotron').append('<h1>' + jumbotronHeaderInput + '</h1>');
                console.clear();
                jumbotronHeaderPreviewCount++;
                console.log('<addJumbotronHeader>');
                console.log('Jumbotron Header applied:');
                if (jumbotronHeaderInput === "") {
                    console.log('Jumbotron Header is empty...');
                    var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>Please fill in the Jumbotron Header Field!'</li> </ul>");
                    $(".container div#custommessages").append(errorWrapper);
                    setTimeout(function() {
                        $(".container div#custommessages ul").slideUp(1000);
                    }, 6000);
                    console.clear();
                    console.log("Error triggered, trigger count = " + errorCount);
                    jumbotronHeaderPreviewCount--;
                } else {
                    console.log(jumbotronHeaderInput);
                }
                console.log('</addJumbotronHeader>');

            } else {
                var errorWrapper2 = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add 1 Jumbotron Header Preview!'</li> </ul>");
                $(".container div#custommessages").append(errorWrapper2);
                setTimeout(function() {
                    $(".container div#custommessages ul").slideUp(1000);
                }, 6000);
                errorCount++;
                console.clear();
                console.log("Error triggered, trigger count = " + errorCount);
            }
        });

        /////////////////////////////////////////////////////////////////
        // unAppend from preview
        /////////////////////////////////////////////////////////////////
        $("button#removeJumbotronHeader").click(function() {
            jumbotronHeaderPreviewCount = 0;
            console.log('</removeJumbotronHeader>');
            $('.container .jumbotron h1').remove();
            console.log('- Jumbotron Header removed.');
            console.log('</removeJumbotronHeader>');
        });

    } else {
        /////////////////////////////////////////////////////////////////
        // Error display
        /////////////////////////////////////////////////////////////////
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + jumbotronHeaderCount + " Jumbotron header!'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendJumbotronParagraph
/////////////////////////////////////////////////////////////////
$("a#appendJumbotronParagraph").click(function() {
    if (jumbotronParagraphCount < 5) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        jumbotronParagraphCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container jumbotronParagraphContainer innerSetting\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h3>Jumbotron Paragraph " + jumbotronParagraphCount + " Settings</h3></div>");
        var fieldLabel = $("<label for=\"Logo-set" + intId + "\">Jumbotron Paragraph " + jumbotronParagraphCount + " Text: </label>");
        var fieldInput = $("<input type=\"text\" id=\"jumbotronParagraphInput\"/ class=\"fieldname\" name=\"appendJumbotronParagraph\"/>");
        var previewButton = $("<button type=\"button\" id=\"addJumbotronParagraph\">Preview Jumbotron Paragraph</button> ");
        var clearButton = $("<button type=\"button\" id=\"removeJumbotronParagraph\">Clear Jumbotron Paragraph</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            jumbotronParagraphCount = 0; //reset count
            console.clear();
            console.log('<unappendJumbotronParagraph>');
            console.log('- IntId: ' + intId);
            console.log('- jumbotronParagraphCount: ' + jumbotronParagraphCount);
            console.log('</unappendJumbotronParagraph>');
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        fieldWrapper.append(fieldLabel);
        fieldWrapper.append(fieldInput);
        fieldWrapper.append(previewButton);
        fieldWrapper.append(clearButton);
        $("#displaySettings .jumbotronContainer").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendJumbotronParagraph>');
        console.log('- IntId: ' + intId);
        console.log('- jumbotronParagraphCount: ' + jumbotronParagraphCount);
        console.log('</appendJumbotronParagraph>');

        /////////////////////////////////////////////////////////////////
        // Live Preview code
        /////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////
        // Append to preview
        /////////////////////////////////////////////////////////////////
        $("button#addJumbotronParagraph").click(function() {
            var jumbotronParagraphInput = $("#jumbotronParagraphInput").val();
            if (jumbotronParagraphPreviewCount === 0) {
                $('.container .jumbotron').append('<p>' + jumbotronParagraphInput + '</p>');
                console.clear();
                jumbotronHeaderPreviewCount++;

                console.log('<addJumbotronParagraph>');
                console.log('Jumbotron Paragraph applied:');
                if (jumbotronParagraphInput === "") {
                    console.log('Jumbotron Paragraph is empty...');
                    var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>Please fill in the Jumbotron Paragraph Field!'</li> </ul>");
                    $(".container div#custommessages").append(errorWrapper);
                    setTimeout(function() {
                        $(".container div#custommessages ul").slideUp(1000);
                    }, 6000);
                    console.clear();
                    console.log("Error triggered, trigger count = " + errorCount);
                    jumbotronParagraphPreviewCount--;
                } else {
                    console.log(jumbotronParagraphInput);
                }
                console.log('</addJumbotronParagraph>');
            } else {
                var errorWrapper2 = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add 1 Jumbotron Paragraph Preview!'</li> </ul>");
                $(".container div#custommessages").append(errorWrapper2);
                setTimeout(function() {
                    $(".container div#custommessages ul").slideUp(1000);
                }, 6000);
                errorCount++;
                console.clear();
                console.log("Error triggered, trigger count = " + errorCount);
            }

        });

        /////////////////////////////////////////////////////////////////
        // unAppend from preview
        /////////////////////////////////////////////////////////////////
        $("button#removeJumbotronParagraph").click(function() {
            jumbotronParagraphPreviewCount = 0;
            console.log('</removeJumbotronParagraph>');
            $('.container .jumbotron p').remove();
            console.log('- Jumbotron Paragraph removed.');
            console.log('</removeJumbotronParagraph>');
        });

    } else {
        /////////////////////////////////////////////////////////////////
        // Error display
        /////////////////////////////////////////////////////////////////
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + jumbotronParagraphCount + " Jumbotron paragraphs!'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendMainContent
/////////////////////////////////////////////////////////////////
$("a#appendMainContent").click(function() {
    if (mainContentCount < 1) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        mainContentCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container mainContentContainer\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h1>Main Content Settings</h1></div>");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            mainContentCount = 0; //reset count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<unappendMainContent>');
            console.log('- IntId: ' + intId);
            console.log('- mainContentCount: ' + mainContentCount);
            $("a#appendMainContentHeader").addClass("disabled");
            $("a#appendMainContentParagraph").addClass("disabled");
            console.log('- Added disabled class to a#appendMainContentHeader & a#appendMainContentParagraph');
            console.log('</unappendMainContent>');
            /////////////////////////////////////////////////////////////////
            // Main content header count check
            /////////////////////////////////////////////////////////////////
            if (mainContentHeaderCount > 0) {
                $(".mainContentHeaderContainer").remove();
                intId--;
                mainContentHeaderCount = 0;
                /////////////////////////////////////////////////////////////////
                // Console Log
                /////////////////////////////////////////////////////////////////
                console.log('<unappendMainContentHeader>');
                console.log('- IntId: ' + intId);
                console.log('- mainContentHeaderCount: ' + mainContentHeaderCount);
                console.log('</unappendMainContentHeader>');
            } else {
                return;
            }
            if (mainContentParagraphCount > 0) {
                /////////////////////////////////////////////////////////////////
                // Console Log
                /////////////////////////////////////////////////////////////////
                console.log('<unappendMainContentParagraph>');
                for (i = 0; i < mainContentParagraphCount; i++) {
                    intId--;
                    console.log('- Removing main content paragraphs');
                }
                console.log('- IntId: ' + intId);
                mainContentParagraphCount = 0;
                console.log('- mainContentParagraphCount: ' + mainContentParagraphCount);
                console.log('</unappendMainContentParagraph>');
            } else {
                return;
            }
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        $("#displaySettings").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendMainContent>');
        console.log('- IntId: ' + intId);
        console.log('- mainContentCount: ' + mainContentCount);
        $("a#appendMainContentHeader").removeClass("disabled");
        $("a#appendMainContentParagraph").removeClass("disabled");
        console.log('- Removed disabled class from a#appendMainContentHeader & a#appendMainContentParagraph');
        console.log('</appendMainContent>');

        /////////////////////////////////////////////////////////////////
        // Live Preview Code
        /////////////////////////////////////////////////////////////////
        console.log('<addMainContent>');
        previewId++;
        var previewWrapper = $("<div class=\"container mainContentPreview\" id=\"preview" + previewId + "\"/ >");
        var previewHolder = $("<div class='mainContent'></div>");

        previewWrapper.append(previewHolder);
        $("#livePreview").append(previewWrapper);

        console.log('- Main Content applied to preview.');
        console.log('- previewId = ' + previewId);
        console.log('</addMainContent>');


    } else {
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + mainContentCount + " Main content area!'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});


/////////////////////////////////////////////////////////////////
// appendMainContentHeader
/////////////////////////////////////////////////////////////////
$("a#appendMainContentHeader").click(function() {
    if (mainContentHeaderCount < 1) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        mainContentHeaderCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container fieldwrapper innerSetting\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h3>Main Content Header Settings</h3></div>");
        var fieldLabel = $("<label for=\"Logo-set" + intId + "\">Main Content Header Text: </label>");
        var fieldInput = $("<input type=\"text\"  id=\"mainContentHeaderInput\"/ class=\"fieldname\" name=\"appendMainContentHeader\"/>");
        var previewButton = $("<button type=\"button\" id=\"addMainContentHeader\">Preview Main Content Header</button> ");
        var clearButton = $("<button type=\"button\" id=\"removeMainContentHeader\">Clear Main Content Header</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            mainContentHeaderCount = 0; //reset count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<appendMainContentHeader>');
            console.log('- IntId: ' + intId);
            console.log('- mainContentHeaderCount: ' + mainContentHeaderCount);
            console.log('</appendMainContentHeader>');
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        fieldWrapper.append(fieldLabel);
        fieldWrapper.append(fieldInput);
        fieldWrapper.append(previewButton);
        fieldWrapper.append(clearButton);
        $("#displaySettings .mainContentContainer").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendMainContentHeader>');
        console.log('- IntId: ' + intId);
        console.log('- mainContentHeaderCount: ' + mainContentHeaderCount);
        console.log('</appendMainContentHeader>');

        /////////////////////////////////////////////////////////////////
        // Live Preview code
        /////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////
        // Append to preview
        /////////////////////////////////////////////////////////////////
        $("button#addMainContentHeader").click(function() {
            var mainContentHeaderInput = $("#mainContentHeaderInput").val();
            if (mainContentHeaderPreviewCount === 0) {
                $('.container .mainContentPreview .mainContent').append('<h1>' + mainContentHeaderInput + '</h1>');
                console.clear();
                jumbotronHeaderPreviewCount++;
                console.log('<addMainContentHeader>');
                console.log('Main Content Header applied:');
                if (mainContentHeaderInput === "") {
                    console.log('Main Content Header is empty...');
                    console.log('Jumbotron Header is empty...');
                    var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>Please fill in the Main Content Paragraph Field!'</li> </ul>");
                    $(".container div#custommessages").append(errorWrapper);
                    setTimeout(function() {
                        $(".container div#custommessages ul").slideUp(1000);
                    }, 6000);
                    console.clear();
                    console.log("Error triggered, trigger count = " + errorCount);
                    jumbotronHeaderPreviewCount--;
                } else {
                    console.log(mainContentHeaderInput);
                }
                console.log('</addMainContentHeader>');
            } else {
                var errorWrapper2 = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add 1 Main Content Paragraph Preview!'</li> </ul>");
                $(".container div#custommessages").append(errorWrapper2);
                setTimeout(function() {
                    $(".container div#custommessages ul").slideUp(1000);
                }, 6000);
                errorCount++;
                console.clear();
                console.log("Error triggered, trigger count = " + errorCount);
            }

        });

        /////////////////////////////////////////////////////////////////
        // unAppend from preview
        /////////////////////////////////////////////////////////////////
        $("button#removeMainContentHeader").click(function() {
            mainContentHeaderPreviewCount = 0;
            console.log('</removeMainContentHeader>');
            $('.container .mainContentPreview .mainContent h1').remove();
            console.log('- Main Content Header removed.');
            console.log('</removeMainContentHeader>');
        });


    } else {
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + mainContentHeaderCount + " Main content header'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendMainContentParagraph
/////////////////////////////////////////////////////////////////
$("a#appendMainContentParagraph").click(function() {
    if (mainContentParagraphCount < 5) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        mainContentParagraphCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container mainContentParagraphContainer innerSetting\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h3>Main Content Paragraph " + mainContentParagraphCount + " Settings</h3></div>");
        var fieldLabel = $("<label for=\"Logo-set" + intId + "\">Main Content Paragraph " + mainContentParagraphCount + " Text: </label>");
        var fieldInput = $("<input type=\"text\" id=\"mainContentParagraphInput\"/ class=\"fieldname\" name=\"appendMainContentParagraph\"/>");
        var previewButton = $("<button type=\"button\" id=\"addMainContentParagraph\">Preview Main Content Paragraph</button> ");
        var clearButton = $("<button type=\"button\" id=\"removeMainContentParagraph\">Clear Main Content Paragraph</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            mainContentParagraphCount = 0; //reset count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<unappendMainContentParagraph>');
            console.log('- IntId: ' + intId);
            console.log('- mainContentParagraphCount: ' + mainContentParagraphCount);
            console.log('</unappendMainContentParagraph>');
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        fieldWrapper.append(fieldLabel);
        fieldWrapper.append(fieldInput);
        fieldWrapper.append(previewButton);
        fieldWrapper.append(clearButton);
        $("#displaySettings .mainContentContainer").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendMainContentParagraph>');
        console.log('- IntId: ' + intId);
        console.log('- mainContentParagraphCount: ' + mainContentParagraphCount);
        console.log('</appendMainContentParagraph>');

        /////////////////////////////////////////////////////////////////
        // Live Preview code
        /////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////
        // Append to preview
        /////////////////////////////////////////////////////////////////
        $("button#addMainContentParagraph").click(function() {
            var mainContentParagraphInput = $("#mainContentParagraphInput").val();
            if (mainContentParagraphPreviewCount === 0) {
                $('.container .mainContentPreview .mainContent').append('<p>' + mainContentParagraphInput + '</p>');
                console.clear();
                mainContentParagraphPreviewCount++;
                console.log('<addMainContentParagraph>');
                console.log('Main Content Paragraph applied:');
                if (mainContentParagraphInput === "") {
                    console.log('Main Content Paragraph is empty...');
                    console.log('Jumbotron Header is empty...');
                    var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>Please fill in the Main Content Paragraph Field!'</li> </ul>");
                    $(".container div#custommessages").append(errorWrapper);
                    setTimeout(function() {
                        $(".container div#custommessages ul").slideUp(1000);
                    }, 6000);
                    console.clear();
                    console.log("Error triggered, trigger count = " + errorCount);
                    mainContentParagraphPreviewCount--;
                } else {
                    console.log(mainContentParagraphInput);
                }
                console.log('</addMainContentParagraph>');
            } else {
                var errorWrapper2 = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add 5 Main Content Paragraph previews!'</li> </ul>");
                $(".container div#custommessages").append(errorWrapper2);
                setTimeout(function() {
                    $(".container div#custommessages ul").slideUp(1000);
                }, 6000);
                errorCount++;
                console.clear();
                console.log("Error triggered, trigger count = " + errorCount);
            }

        });

        /////////////////////////////////////////////////////////////////
        // unAppend from preview
        /////////////////////////////////////////////////////////////////
        $("button#removeMainContentParagraph").click(function() {
			mainContentParagraphPreviewCount = 0;
            console.log('</removeMainContentParagraph>');
            $('.container .mainContentPreview .mainContent p').remove();
            console.log('- Main Content Paragraph removed.');
            console.log('</removeMainContentParagraph>');
        });

    } else {
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + mainContentParagraphCount + " Main content header'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendFooter
/////////////////////////////////////////////////////////////////
$("a#appendFooter").click(function() {
    if (footerCount < 3) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        footerCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container footerContainer fieldwrapper\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h1>Footer Settings</h1></div>");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            footerCount = 0; //reset count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<unappendFooter>');
            console.log('- IntId: ' + intId);
            console.log('- footerCount: ' + footerCount);
            $("a#appendFooterSection").addClass("disabled");
            console.log('- Added disabled class to a#appendFooterSection & a#appendFooterLink');
            console.log('</unappendFooter>');
            /////////////////////////////////////////////////////////////////
            // Footer section count check
            /////////////////////////////////////////////////////////////////
            if (footerSectionCount > 0) {
                /////////////////////////////////////////////////////////////////
                // Console Log
                /////////////////////////////////////////////////////////////////
                console.log('<unappendFooterSection>');
                for (i = 0; i < footerSectionCount; i++) {
                    intId--;
                    console.log('- Removing footer sections');
                }
                console.log('- IntId: ' + intId);
                footerSectionCount = 0;
                console.log('- footerSectionCount: ' + footerSectionCount);
                $("a#appendFooterSection").addClass("disabled");
                console.log('- Added disabled class to a#appendFooterSection');
                console.log('</unappendFooterSection>');
            } else {
                return;
            }
            if (footerLinkCount > 0) {
                /////////////////////////////////////////////////////////////////
                // Console Log
                /////////////////////////////////////////////////////////////////
                console.log('<unappendFooterLink>');
                for (i = 0; i < footerLinkCount; i++) {
                    intId--;
                    console.log('- Removing footer links');
                }
                console.log('- IntId: ' + intId);
                footerLinkCount = 0;
                console.log('- footerLinkCount: ' + footerLinkCount);
                $("a#appendFooterLink").addClass("disabled");
                console.log('- Added disabled class to a#appendLogoLink');
                console.log('</unappendFooterLink>');
            } else {
                return;
            }
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        $("#displaySettings").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendFooter>');
        console.log('- IntId: ' + intId);
        console.log('- footerCount: ' + footerCount);
        $("a#appendFooterSection").removeClass("disabled");
        console.log('- Removed disabled class from a#appendFooterSection');
        console.log('</appendFooter>');

        /////////////////////////////////////////////////////////////////
        // Live Preview Code
        /////////////////////////////////////////////////////////////////
        console.log('<addFooter>');
        previewId++;
        var previewWrapper = $("<div class=\"container footerPreview\" id=\"preview" + previewId + "\"/ >");
        var previewHolder = $("<div class='footerContent'></div>");

        previewWrapper.append(previewHolder);
        $("#livePreview").append(previewWrapper);

        console.log('- Footer applied to preview.');
        console.log('- previewId = ' + previewId);
        console.log('</addFooter>');


    } else {
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + footerCount + " Footer'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendFooterSection
/////////////////////////////////////////////////////////////////
$("a#appendFooterSection").click(function() {
    if (footerSectionCount < 3) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        footerSectionCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container footerSectionContainer" + footerSectionCount + " innerSetting\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h3>Footer Section " + footerSectionCount + " Settings</h3></div>");
        var fieldLabel = $("<label for=\"Logo-set" + intId + "\">Footer Section " + footerSectionCount + " Text: </label>");
        var fieldInput = $("<input type=\"text\" id=\"footerSectionInput\"/ class=\"fieldname\" name=\"appendFooterSection\"/>");
        var previewButton = $("<button type=\"button\" id=\"addFooterSection\">Preview Footer Section</button> ");
        var clearButton = $("<button type=\"button\" id=\"removeFooterSection\">Clear Footer Section</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            footerSectionCount = 0; //reset count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<unappendFooterSection>');
            console.log('- IntId: ' + intId);
            console.log('- footerSectionCount: ' + footerSectionCount);
            $("a#appendFooterLink").addClass("disabled");
            console.log('- Added disabled class from a#appendFooterLink');
            console.log('</unappendFooterSection>');
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        fieldWrapper.append(fieldLabel);
        fieldWrapper.append(fieldInput);
        fieldWrapper.append(previewButton);
        fieldWrapper.append(clearButton);
        $("#displaySettings .footerContainer").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendFooterSection>');
        console.log('- IntId: ' + intId);
        console.log('- footerSectionCount: ' + footerSectionCount);
        $("a#appendFooterLink").removeClass("disabled");
        console.log('- Removed disabled class from a#appendFooterLink');
        console.log('</appendFooterSection>');

        /////////////////////////////////////////////////////////////////
        // Live Preview code
        /////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////
        // Append to preview
        /////////////////////////////////////////////////////////////////
        $("button#addFooterSection").click(function() {
            var footerSectionInput = $("#footerSectionInput").val();
            console.log('<addFooterSection>');
            console.log('Footer Section applied:');
            if (footerSectionInput === "") {
                console.log('Footer Section is empty...');
            } else {
                console.log(footerSectionInput);
            }
            console.log('</addMainContentParagraph>');
            $('.container .footerPreview .footerContent').append('<p>' + footerSectionInput + '</p>');
        });

        /////////////////////////////////////////////////////////////////
        // unAppend from preview
        /////////////////////////////////////////////////////////////////
        $("button#removeFooterSection").click(function() {
            console.log('</removeFooterSection>');
            $('.container .footerPreview .footerContent p').remove();
            console.log('- Footer Section removed.');
            console.log('</removeFooterSection>');
        });


    } else {
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + footerSectionCount + " Footer'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});

/////////////////////////////////////////////////////////////////
// appendFooterLink
/////////////////////////////////////////////////////////////////
$("a#appendFooterLink").click(function() {
    if (footerLinkCount < 15) {
        /////////////////////////////////////////////////////////////////
        // Counts added
        /////////////////////////////////////////////////////////////////
        intId++;
        footerLinkCount++;

        /////////////////////////////////////////////////////////////////
        // Content to be appended
        /////////////////////////////////////////////////////////////////
        var fieldWrapper = $("<div class=\"container footerLinkContainer" + footerLinkCount + " innerSetting\" id=\"field" + intId + "\"/ >");
        var contentHolder = $("<div class='headerItems'><h3>Footer Link " + footerLinkCount + " Settings</h3></div>");
        var fieldLabel = $("<label for=\"Logo-set" + intId + "\">Footer Link " + footerLinkCount + " Link: </label>");
        var fieldInput = $("<input type=\"text\" id=\"footerLinkInput\"/ class=\"fieldname\" name=\"appendFooterLink\"/>");
        var previewButton = $("<button type=\"button\" id=\"addFooterLink\">Preview Footer Link</button> ");
        var clearButton = $("<button type=\"button\" id=\"removeFooterLink\">Clear Footer Link</button> ");
        var removeButton = $("<div class='headerItems'><span class=\"cursor-pointer glyphicon glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></div>");

        /////////////////////////////////////////////////////////////////
        // Remove button
        /////////////////////////////////////////////////////////////////
        removeButton.click(function() {
            $(this).parent().remove(); //remove parent
            intId--; //remove count
            footerSectionCount = 0; //reset count
            /////////////////////////////////////////////////////////////////
            // Console Log
            /////////////////////////////////////////////////////////////////
            console.clear();
            console.log('<unappendFooterLink>');
            console.log('- IntId: ' + intId);
            console.log('- footerSectionCount: ' + footerLinkCount);
            console.log('</unappendFooterLink>');
        });

        /////////////////////////////////////////////////////////////////
        // Appending content
        /////////////////////////////////////////////////////////////////
        fieldWrapper.append(fieldWrapper);
        fieldWrapper.append(contentHolder);
        fieldWrapper.append(removeButton);
        fieldWrapper.append(fieldLabel);
        fieldWrapper.append(fieldInput);
        fieldWrapper.append(previewButton);
        fieldWrapper.append(clearButton);
        $("#displaySettings .footerContainer").append(fieldWrapper);

        /////////////////////////////////////////////////////////////////
        // Console Log
        /////////////////////////////////////////////////////////////////
        console.clear();
        console.log('<appendFooterLink>');
        console.log('- IntId: ' + intId);
        console.log('- footerLinkCount: ' + footerLinkCount);
        console.log('</appendFooterLink>');

        /////////////////////////////////////////////////////////////////
        // Live Preview code
        /////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////
        // Append to preview
        /////////////////////////////////////////////////////////////////
        $("button#addFooterLink").click(function() {
            var footerLinkInput = $("#footerLinkInput").val();
            console.log('<addFooterLink>');
            console.log('Footer Link applied:');
            if (footerLinkInput === "") {
                console.log('Footer Link is empty...');
            } else {
                console.log(footerLinkInput);
            }
            console.log('</addFooterLink>');
            $('.container .footerPreview .footerContent').append('<p>' + footerLinkInput + '</p>');
        });

        /////////////////////////////////////////////////////////////////
        // unAppend from preview
        /////////////////////////////////////////////////////////////////
        $("button#removeFooterLink").click(function() {
            console.log('</removeFooterSection>');
            $('.container .footerPreview .footerContent p').remove();
            console.log('- Footer Link removed.');
            console.log('</removeFooterLink>');
        });


    } else {
        var errorWrapper = $("<ul class=\'errorCount" + errorCount + " alert alert-danger\'><li>You can only add " + footerLinkCount + " Footer'</li> </ul>");
        $(".container div#custommessages").append(errorWrapper);
        setTimeout(function() {
            $(".container div#custommessages ul").slideUp(1000);
        }, 6000);
        errorCount++;
        console.clear();
        console.log("Error triggered, trigger count = " + errorCount);
    }
});